#pragma once


namespace Matrix {


	template<class T, class G> double Dist(const T* a, const G* b, int n) {
		double result = 0;
		for (auto i = 0; i < n; i++)
			result += (a[i] - b[i])*(a[i] - b[i]);
		return sqrt(result);
	}


	template<class T> void Zero(T* a, int n) {
		for (auto i = 0; i < n; i++)
			a[i] = 0;
	}


	template<class T> void Zero(T** a, int n) {
		for (auto i = 0; i < n; i++)
			for (auto j = 0; j < n; j++)
				a[i][j] = 0;
	}


	template<class T, class G> void Add(T* a, const G* b, int n) {
		for (auto i = 0; i < n; i++)
			a[i] += b[i];
	}


	template<class T, class G> void AddWeight(T* a, const G* b, double w, int n) {
		for (auto i = 0; i < n; i++)
			a[i] += b[i] * w;
	}


	template<class T, class G> void AddWeightDynamicMatrixSub(T** a, double w, const G* x, const T* y, int n) {
		for (auto i = 0; i < n; i++)
			for (auto j = 0; j < n; j++)
				a[i][j] += w*(x[i] - y[i])*(x[j] - y[j]);
	}


	template<class T> void Div(double* a, T b, int n) {
		for (auto i = 0; i < n; i++)
			a[i] /= b;
	}


	template<class T> void Div(double** a, T b, int n) {
		for (auto i = 0; i < n; i++)
			for (auto j = 0; j < n; j++)
				a[i][j] /= b;
	}


	template<class T, class G> double MultMatrixDynamicVectorSub(const T* const* a, const G* x, const T* y, int n) {
		double result = 0;
		for (auto i = 0; i < n; i++) {
			double s = 0;
			for (auto j = 0; j < n; j++) {
				s += (x[j] - y[j])*a[j][i];
			}
			result += (x[i] - y[i])*s;
		}
		return result;
	}

};

