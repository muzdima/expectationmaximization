#pragma once
#include "IReader.h"


class ReaderMnist : public IReader
{
public:
	ReaderMnist(bool isTrain, int corpusSizeMax = 0);
	virtual ~ReaderMnist();
	int GetCorpusSize() const;
	int GetVectorSize() const;
	const unsigned char* const* GetData() const;
	const char* GetLabels() const;
	int GetClasses() const;
private:
	int corpusSize, vectorSize;
	unsigned char** data;
	char* labels;
};

