#pragma once
#include "IReader.h"


class ReaderBinary : public IReader
{
public:
	ReaderBinary(IReader* reader, unsigned char threshold);
	virtual ~ReaderBinary();
	int GetCorpusSize() const;
	int GetVectorSize() const;
	const unsigned char* const* GetData() const;
	const char* GetLabels() const;
	int GetClasses() const;
private:
	int corpusSize, vectorSize;
	unsigned char** data;
	char* labels;
	int classes;
};

