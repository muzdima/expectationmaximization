#include "ReaderBinary.h"
#include <memory>


ReaderBinary::ReaderBinary(IReader* reader, unsigned char threshold) {
	corpusSize = reader->GetCorpusSize();
	vectorSize = reader->GetVectorSize();
	auto dataReader = reader->GetData();
	data = new unsigned char*[corpusSize];
	for (auto i = 0; i < corpusSize; i++) {
		data[i] = new unsigned char[vectorSize];
		for (auto j = 0; j < vectorSize; j++)
			if (dataReader[i][j] >= threshold)
				data[i][j] = 1;
			else
				data[i][j] = 0;
	}
	labels = new char[corpusSize];
	memcpy(labels, reader->GetLabels(), vectorSize);
	classes = reader->GetClasses();
}


ReaderBinary::~ReaderBinary() {
	for (auto i = 0; i < corpusSize; i++)
		delete[] data[i];
	delete[] data;
	data = nullptr;
	delete[] labels;
	labels = nullptr;
}


int ReaderBinary::GetCorpusSize() const {
	return corpusSize;
}


int ReaderBinary::GetVectorSize() const {
	return vectorSize;
}


const unsigned char* const* ReaderBinary::GetData() const {
	return data;
}


const char* ReaderBinary::GetLabels() const {
	return labels;
}


int ReaderBinary::GetClasses() const {
	return classes;
}
