#define _SCL_SECURE_NO_WARNINGS
#include "MapperSimple.h"
#include <assert.h>
#include <set>
#include <map>
#include <vector>
#include <algorithm>


MapperSimple::MapperSimple(IReader* reader, IClasterizer* clasterizer) :reader(reader), clasterizer(clasterizer) {
	auto labelsReader = reader->GetLabels();
	std::set<char> labelsSetReader(labelsReader, labelsReader + reader->GetCorpusSize());
	auto labelsClasterizer = clasterizer->GetLabels();
	std::set<char> labelsSetClasterizer(labelsClasterizer, labelsClasterizer + clasterizer->GetCorpusSize());
	assert(labelsSetReader.size() >= labelsSetClasterizer.size());
	
	while (labelsSetReader.size() != labelsSetClasterizer.size()) {
		labelsSetClasterizer.insert(char(rand()));
	}

	labelListSize = labelsSetReader.size();
	labelListReader = new char[labelListSize];
	auto labelListClasterizer = new char[labelListSize];

	std::copy(labelsSetReader.begin(), labelsSetReader.end(), labelListReader);
	std::copy(labelsSetClasterizer.begin(), labelsSetClasterizer.end(), labelListClasterizer);

	assert(reader->GetCorpusSize() == clasterizer->GetCorpusSize());
	auto n = reader->GetCorpusSize();

	std::map<char, std::map<char, int>> labelsReaderAsClasterizerMap;
	for (auto i = 0; i < n; i++) {
		auto labelReader = labelsReader[i];
		auto labelClasterizer = labelsClasterizer[i];
		labelsReaderAsClasterizerMap[labelReader][labelClasterizer]++;
	}

	std::vector<std::pair<int, std::pair<char, char>>>  labelsReaderAsClasterizerVector;
	for (auto labelReaderIndex = 0; labelReaderIndex < labelListSize; labelReaderIndex++)
		for (auto labelClasterizerIndex = 0; labelClasterizerIndex < labelListSize; labelClasterizerIndex++) {
			auto labelReader = labelListReader[labelReaderIndex];
			auto labelClasterizer = labelListClasterizer[labelClasterizerIndex];
			auto value = labelsReaderAsClasterizerMap[labelReader][labelClasterizer];
			labelsReaderAsClasterizerVector.push_back(std::make_pair(value, std::make_pair(labelReader, labelClasterizer)));
		}
	std::sort(labelsReaderAsClasterizerVector.begin(), labelsReaderAsClasterizerVector.end());
	std::reverse(labelsReaderAsClasterizerVector.begin(), labelsReaderAsClasterizerVector.end());

	std::map<char, char> readerToClasterizerLabel;
	for (auto item : labelsReaderAsClasterizerVector) {
		auto labelReader = item.second.first;
		auto labelClasterizer = item.second.second;
		if (clasterizerToReaderLabel.find(labelClasterizer) == clasterizerToReaderLabel.end()
			&& readerToClasterizerLabel.find(labelReader) == readerToClasterizerLabel.end()) {
			clasterizerToReaderLabel[labelClasterizer] = labelReader;
			readerToClasterizerLabel[labelReader] = labelClasterizer;
		}
	}

	matrix = new int*[labelListSize];
	for (auto i = 0; i < labelListSize; i++) {
		matrix[i] = new int[labelListSize];
		for (auto j = 0; j < labelListSize; j++)
			matrix[i][j] = labelsReaderAsClasterizerMap[labelListReader[i]][readerToClasterizerLabel[labelListReader[j]]];
	}


	delete[] labelListClasterizer;
	labelListClasterizer = nullptr;
}


MapperSimple::~MapperSimple() {
	delete[] labelListReader;
	labelListReader = nullptr;
	for (auto i = 0; i < labelListSize; i++)
		delete[] matrix[i];
	delete[] matrix;
	matrix = nullptr;
}


int MapperSimple::GetLabelListSize() const {
	return labelListSize;
}


char MapperSimple::MapClasterizerToReaderLabel(char label) const {
	auto result = clasterizerToReaderLabel.find(label);
	assert(result != clasterizerToReaderLabel.end());
	return result->second;
}


const char* MapperSimple::GetLabelList() const {
	return labelListReader;
}


const int* const* MapperSimple::GetConfusionMatrix() const {
	return matrix;
}