#pragma once
#include "IClasterizer.h"
#include "IReader.h"


class ClasterizerEMGaussian : public IClasterizer
{
public:
	ClasterizerEMGaussian(const char* modelPath, IReader* test);
	ClasterizerEMGaussian(IReader* train, IReader* test);
	virtual ~ClasterizerEMGaussian();
	int GetCorpusSize() const;
	const char* GetLabels() const;
	void SaveModel(const char* modelPath);
private:
	int corpusSize;
	char* labels;

	int classSize;
	int vectorSize;
	double* weights;
	double** locations;
	double** covariances;

	void TestModel(IReader* test);
};

