#include "ClasterizerKMeans.h"
#include <vector>
#include "Matrix.h"
#include <assert.h>

ClasterizerKMeans::ClasterizerKMeans(const char * modelPath, IReader * test){
	FILE* f;
	fopen_s(&f, modelPath, "rb");
	char buf[4];
	fread_s(buf, 4, 1, 3, f);
	assert(buf[0] == 'M' && buf[1] == 'U' && buf[2] == 'Z');
	fread_s(&classSize, 4, 4, 1, f);
	fread_s(&vectorSize, 4, 4, 1, f);

	means = new double*[classSize];
	for (auto i = 0; i < classSize; i++)
		means[i] = new double[vectorSize];

	for (auto i = 0; i < classSize; i++)
		fread_s(means[i], vectorSize * sizeof(double), sizeof(double), vectorSize, f);

	fclose(f);

	assert(vectorSize == test->GetVectorSize());
	TestModel(test);
}

void ClasterizerKMeans::SaveModel(const char* modelPath) {
	FILE* f;
	fopen_s(&f, modelPath, "wb");
	fwrite("MUZ", 1, 3, f);
	fwrite(&classSize, 4, 1, f);
	fwrite(&vectorSize, 4, 1, f);
	for (auto i = 0; i < classSize; i++)
		fwrite(means[i], sizeof(double), vectorSize, f);
	fclose(f);
}

void ClasterizerKMeans::TestModel(IReader* test) {
	corpusSize = test->GetCorpusSize();
	auto data = test->GetData();
	labels = new char[corpusSize];
	memset(labels, 0, sizeof(char)*corpusSize);
	for (auto i = 0; i < corpusSize; i++)
		for (auto j = 0; j < classSize; j++)
			if (Matrix::Dist(data[i], means[j], vectorSize) < Matrix::Dist(data[i], means[labels[i]], vectorSize))
				labels[i] = j;
}

ClasterizerKMeans::ClasterizerKMeans(IReader* train, IReader* test) {
	assert(train->GetVectorSize() == test->GetVectorSize());
	vectorSize = train->GetVectorSize();
	classSize = train->GetClasses();
	means = new double*[classSize];
	for (auto i = 0; i < classSize; i++) {
		means[i] = new double[vectorSize];
		for (auto j = 0; j < vectorSize; j++)
			means[i][j] = double(rand()) / RAND_MAX;
	}
	auto n = train->GetCorpusSize();
	auto data = train->GetData();
	auto nearClass = new int[n];
	memset(nearClass, 0, sizeof(int)*n);
	auto nearClassCount = new int[classSize];
	bool isEnd = false;
	auto iteration = 1;
	while (!isEnd) {
		isEnd = true;
		auto points = 0;
		for (auto i = 0; i < n; i++) {
			bool change = false;
			for (auto j = 0; j < classSize; j++)
				if (Matrix::Dist(data[i], means[j], vectorSize) < Matrix::Dist(data[i], means[nearClass[i]], vectorSize)) {
					nearClass[i] = j;
					isEnd = false;
					change = true;
				}
			if (change)
				points++;
		}
		printf("Iteration #%-3d: %5d points change class\n", iteration, points);
		for (auto i = 0; i < classSize; i++)
			Matrix::Zero(means[i], vectorSize);
		memset(nearClassCount, 0, sizeof(int)*classSize);
		for (auto i = 0; i < n; i++) {
			Matrix::Add(means[nearClass[i]], data[i], vectorSize);
			nearClassCount[nearClass[i]]++;
		}
		for (auto i = 0; i < classSize; i++)
			Matrix::Div(means[i], nearClassCount[i], vectorSize);
		iteration++;
	}
	delete[] nearClass;
	delete[] nearClassCount;

	TestModel(test);
}


ClasterizerKMeans::~ClasterizerKMeans() {
	delete[] labels;
	labels = nullptr;

	for (auto i = 0; i < classSize; i++)
		delete[] means[i];
	delete[] means;
}


int ClasterizerKMeans::GetCorpusSize() const {
	return corpusSize;
}


const char* ClasterizerKMeans::GetLabels() const {
	return labels;
}