#pragma once


class IMapper
{
public:
	virtual int GetLabelListSize() const = 0;
	virtual char MapClasterizerToReaderLabel(char label) const = 0;
	virtual const char* GetLabelList() const = 0;
	virtual const int* const* GetConfusionMatrix() const = 0;
	virtual ~IMapper() {}
};

