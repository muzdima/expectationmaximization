#include "ClasterizerEMGaussian.h"
#include <assert.h>
#include <vector>
#include "Matrix.h"
#include <string>


//#include <float.h>
//unsigned int fp_control_state = _controlfp(_EM_INEXACT, _MCW_EM);


#define PI 3.1415926535897932384626433832795
#define E 2.7182818284590452353602874713527


static double SubMin(const unsigned char* data, const double* const* location, const double* const* covariance, int n, int classSize) {
	auto subMin = 0.0;
	for (auto k = 0; k < classSize; k++) {
		auto covarianceDetSqrt = 1.0;
		auto sub = 0.0;
		for (auto i = 0; i < n; i++) {
			covarianceDetSqrt *= 1.0 / sqrt(covariance[k][i]);
			while (covarianceDetSqrt < 1.E-10) {
				covarianceDetSqrt *= E;
				sub += 2;
			}
			while (covarianceDetSqrt > 1.E+10) {
				covarianceDetSqrt /= E;
				sub -= 2;
			}
		}
		for (auto i = 0; i < n; i++)
			if (covariance[k][i] > 0)
				sub += (data[i] - location[k][i])*(data[i] - location[k][i]) / covariance[k][i];
		if (subMin == 0 || subMin > abs(sub))
			subMin = abs(sub);
	}
	return subMin;
}

static double PDF(const unsigned char* data, const double* location, const double* covariance, int n, double subMin) {
	auto covarianceDetSqrt = 1.0;
	auto sub = 0.0;
	for (auto i = 0; i < n; i++) {
		covarianceDetSqrt *= 1.0 / sqrt(covariance[i]);
		while (covarianceDetSqrt < 1.E-10) {
			covarianceDetSqrt *= E;
			sub += 2;
		}
		while (covarianceDetSqrt > 1.E+10) {
			covarianceDetSqrt /= E;
			sub -= 2;
		}
	}
	for (auto i = 0; i < n; i++)
		if (covariance[i] > 0)
			sub += (data[i] - location[i])*(data[i] - location[i]) / covariance[i];
	auto val = 0.5*subMin - 0.5*sub;
	if (val < -50) val = -50;
	if (val > 50) val = 50;
	return covarianceDetSqrt*pow(2.0*PI, -0.5)*pow(E, val);
}


ClasterizerEMGaussian::ClasterizerEMGaussian(const char* modelPath, IReader* test) {
	FILE* f;
	fopen_s(&f, modelPath, "rb");
	char buf[4];
	fread_s(buf, 4, 1, 3, f);
	assert(buf[0] == 'M' && buf[1] == 'U' && buf[2] == 'Z');
	fread_s(&classSize, 4, 4, 1, f);
	fread_s(&vectorSize, 4, 4, 1, f);

	weights = new double[classSize];
	fread_s(weights, classSize * sizeof(double), sizeof(double), classSize, f);

	locations = new double*[classSize];
	for (auto i = 0; i < classSize; i++)
		locations[i] = new double[vectorSize];
	for (auto i = 0; i < classSize; i++)
		fread_s(locations[i], vectorSize * sizeof(double), sizeof(double), vectorSize, f);

	covariances = new double*[classSize];
	for (auto i = 0; i < classSize; i++)
		covariances[i] = new double[vectorSize];
	for (auto i = 0; i < classSize; i++)
		fread_s(covariances[i], vectorSize * sizeof(double), sizeof(double), vectorSize, f);

	fclose(f);

	assert(vectorSize == test->GetVectorSize());
	TestModel(test);
}


ClasterizerEMGaussian::ClasterizerEMGaussian(IReader* train, IReader* test) {
	assert(train->GetVectorSize() == test->GetVectorSize());
	vectorSize = train->GetVectorSize();
	classSize = train->GetClasses();
	auto n = train->GetCorpusSize();
	auto data = train->GetData();

	weights = new double[classSize];
	for (auto i = 0; i < classSize; i++)
		weights[i] = 1.0 / classSize;
	locations = new double*[classSize];
	for (auto i = 0; i < classSize; i++) {
		locations[i] = new double[vectorSize];
		for (auto j = 0; j < vectorSize; j++)
			if (rand() % 2)
				locations[i][j] = 255.0;
			else
				locations[i][j] = 0.0;
	}
	covariances = new double*[classSize];
	for (auto i = 0; i < classSize; i++) {
		covariances[i] = new double[vectorSize];
		for (auto j = 0; j < vectorSize; j++) {
			covariances[i][j] = 100.0;
		}
	}

	auto p = new double*[classSize];
	for (auto i = 0; i < classSize; i++)
		p[i] = new double[n];

	auto iteration = 1;
	auto likelihoodLast = 0.0;
	auto isEnd = false;
	while (!isEnd)
	{

		//E
		for (auto i = 0; i < n; i++) {
			auto subMin = SubMin(data[i], locations, covariances, vectorSize, classSize);
			double spi = 0;
			for (auto j = 0; j < classSize; j++) {
				p[j][i] = weights[j] * PDF(data[i], locations[j], covariances[j], vectorSize, subMin);
				spi += p[j][i];
			}
			for (auto j = 0; j < classSize; j++)
				p[j][i] /= spi;
			for (auto j = 0; j < classSize; j++)
				if (p[j][i] < 1.E-10)
					p[j][i] = 1.E-10;
		}

		auto likelihood = 0.0;
		for (auto i = 0; i < n; i++) {
			auto si = 0.0;
			for (auto j = 0; j < classSize; j++)
				si += p[j][i] * PDF(data[i], locations[j], covariances[j], vectorSize, 0);
			likelihood += log(si);
		}
		printf("Iteration #%-3d: Log likelihood %-12.5lf\n", iteration, likelihood);
		if (abs(likelihood - likelihoodLast) < 0.0001) isEnd = true;
		likelihoodLast = likelihood;

		//M - weights
		double sp = 0;
		for (auto i = 0; i < classSize; i++) {
			weights[i] = 0;
			for (auto j = 0; j < n; j++) {
				weights[i] += p[i][j];
				sp += p[i][j];
			}
		}
		Matrix::Div(weights, sp, classSize);
		for (auto i = 0; i < classSize; i++)
			if (weights[i] < 0.01) weights[i] = 0.01;

		//M - locations
		for (auto i = 0; i < classSize; i++) {
			Matrix::Zero(locations[i], vectorSize);
			double spi = 0;
			for (auto j = 0; j < n; j++) {
				Matrix::AddWeight(locations[i], data[j], p[i][j], vectorSize);
				spi += p[i][j];
			}
			Matrix::Div(locations[i], spi, vectorSize);
		}

		//M - covariances
		for (auto i = 0; i < classSize; i++) {
			Matrix::Zero(covariances[i], vectorSize);
			double spi = 0;
			for (auto j = 0; j < n; j++) {
				for (auto k = 0; k < vectorSize; k++)
					covariances[i][k] += p[i][j] * (data[j][k] - locations[i][k])* (data[j][k] - locations[i][k]);
				spi += p[i][j];
			}
			Matrix::Div(covariances[i], n*spi, vectorSize);
			for (auto k = 0; k < vectorSize; k++)
				if (covariances[i][k] < 1.0) covariances[i][k] = 1.0;
		}

		//SaveModel((std::to_string(iteration) + " " + std::to_string(likelihood)).c_str());
		iteration++;
	}

	for (auto i = 0; i < classSize; i++)
		delete[] p[i];
	delete[] p;

	TestModel(test);
}


void ClasterizerEMGaussian::SaveModel(const char* modelPath) {
	FILE* f;
	fopen_s(&f, modelPath, "wb");
	fwrite("MUZ", 1, 3, f);
	fwrite(&classSize, 4, 1, f);
	fwrite(&vectorSize, 4, 1, f);
	fwrite(weights, sizeof(double), classSize, f);
	for (auto i = 0; i < classSize; i++)
		fwrite(locations[i], sizeof(double), vectorSize, f);
	for (auto i = 0; i < classSize; i++)
		fwrite(covariances[i], sizeof(double), vectorSize, f);
	fclose(f);
}


void ClasterizerEMGaussian::TestModel(IReader* test) {
	corpusSize = test->GetCorpusSize();
	auto data = test->GetData();
	labels = new char[corpusSize];

	for (auto i = 0; i < corpusSize; i++) {
		auto subMin = SubMin(data[i], locations, covariances, vectorSize, classSize);
		labels[i] = 0;
		auto pMax = 0.0;
		for (auto j = 0; j < classSize; j++) {
			auto p = weights[j] * PDF(data[i], locations[j], covariances[j], vectorSize, subMin);
			if (pMax < p) {
				labels[i] = j;
				pMax = p;
			}
		}
	}
}


ClasterizerEMGaussian::~ClasterizerEMGaussian() {
	delete[] labels;
	labels = nullptr;

	delete[] weights;
	weights = nullptr;
	for (auto i = 0; i < classSize; i++)
		delete[] locations[i];
	delete[] locations;
	locations = nullptr;
	for (auto i = 0; i < classSize; i++)
		delete[] covariances[i];
	delete[] covariances;
	covariances = nullptr;
}


int ClasterizerEMGaussian::GetCorpusSize() const {
	return corpusSize;
}


const char* ClasterizerEMGaussian::GetLabels() const {
	return labels;
}