#include "ReaderMnist.h"
#include <stdio.h>
#include <assert.h>

static inline void endianSwap(int& x)
{
	x = (x >> 24) |
		((x << 8) & 0x00FF0000) |
		((x >> 8) & 0x0000FF00) |
		(x << 24);
}

ReaderMnist::ReaderMnist(bool isTrain, int corpusSizeMax) {
	auto imagesPath = "../data/t10k-images.idx3-ubyte";
	auto labelsPath = "../data/t10k-labels.idx1-ubyte";
	if (isTrain) {
		imagesPath = "../data/train-images.idx3-ubyte";
		labelsPath = "../data/train-labels.idx1-ubyte";
	}
	FILE* imagesFile;
	FILE* labelsFile;
	fopen_s(&imagesFile, imagesPath, "rb");
	if (!isTrain) fopen_s(&labelsFile, labelsPath, "rb");

	int imagesMagic, imagesSize, imagesRows, imagesColumns;
	fread_s(&imagesMagic, 4, 4, 1, imagesFile);
	bool imagesSwap = false;
	if (imagesMagic != 2051) imagesSwap = true;

	int labelsMagic = 0, labelsSize = 0;
	if (!isTrain) fread_s(&labelsMagic, 4, 4, 1, labelsFile);
	bool labelsSwap = false;
	if (labelsMagic != 2049) labelsSwap = true;

	fread_s(&imagesSize, 4, 4, 1, imagesFile);
	fread_s(&imagesRows, 4, 4, 1, imagesFile);
	fread_s(&imagesColumns, 4, 4, 1, imagesFile);

	if (imagesSwap) {
		endianSwap(imagesSize);
		endianSwap(imagesRows);
		endianSwap(imagesColumns);
	}

	if (!isTrain) fread_s(&labelsSize, 4, 4, 1, labelsFile);

	if (labelsSwap) {
		endianSwap(labelsSize);
	}

	assert(imagesRows == 28);
	assert(imagesColumns == 28);
	if (!isTrain) assert(imagesSize == labelsSize);
	corpusSize = imagesSize;
	if (corpusSizeMax > 0 && corpusSize > corpusSizeMax)
		corpusSize = corpusSizeMax;
	vectorSize = imagesRows*imagesColumns;

	data = new unsigned char*[corpusSize];
	labels = new char[corpusSize];

	for (auto i = 0; i < corpusSize; i++) {
		data[i] = new unsigned char[vectorSize];
		fread_s(data[i], vectorSize, 1, vectorSize, imagesFile);
	}

	if (!isTrain) fread_s(labels, corpusSize, 1, corpusSize, labelsFile);

	fclose(imagesFile);
	if (!isTrain) fclose(labelsFile);
}


ReaderMnist::~ReaderMnist() {
	for (auto i = 0; i < corpusSize; i++)
		delete[] data[i];
	delete[] data;
	data = nullptr;
	delete[] labels;
	labels = nullptr;
}


int ReaderMnist::GetCorpusSize() const {
	return corpusSize;
}


int ReaderMnist::GetVectorSize() const {
	return vectorSize;
}


const unsigned char* const* ReaderMnist::GetData() const {
	return data;
}


const char* ReaderMnist::GetLabels() const {
	return labels;
}


int ReaderMnist::GetClasses() const {
	return 10;
}