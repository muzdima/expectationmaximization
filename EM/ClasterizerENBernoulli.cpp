#include "ClasterizerENBernoulli.h"
#include <assert.h>
#include <vector>
#include <string>


//#include <float.h>
//unsigned int fp_control_state = _controlfp(_EM_INEXACT, _MCW_EM);


ClasterizerEMBernoulli::ClasterizerEMBernoulli(const char* modelPath, IReader* test) {
	FILE* f;
	fopen_s(&f, modelPath, "rb");
	char buf[4];
	fread_s(buf, 4, 1, 3, f);
	assert(buf[0] == 'M' && buf[1] == 'U' && buf[2] == 'Z');
	fread_s(&classSize, 4, 4, 1, f);
	fread_s(&vectorSize, 4, 4, 1, f);

	weights = new double[classSize];

	fread_s(weights, classSize * sizeof(double), sizeof(double), classSize, f);

	probabilities = new double*[classSize];
	for (auto i = 0; i < classSize; i++)
		probabilities[i] = new double[vectorSize];

	for (auto i = 0; i < classSize; i++)
		fread_s(probabilities[i], vectorSize * sizeof(double), sizeof(double), vectorSize, f);

	fclose(f);

	assert(vectorSize == test->GetVectorSize());
	TestModel(test);
}


ClasterizerEMBernoulli::ClasterizerEMBernoulli(IReader* train, IReader* test) {
	assert(train->GetVectorSize() == test->GetVectorSize());
	vectorSize = train->GetVectorSize();
	classSize = train->GetClasses();
	auto n = train->GetCorpusSize();
	auto data = train->GetData();

	weights = new double[classSize];
	for (auto i = 0; i < classSize; i++)
		weights[i] = 1.0/classSize;
	probabilities = new double*[classSize];
	for (auto i = 0; i < classSize; i++) {
		probabilities[i] = new double[vectorSize];
		for (auto j = 0; j < vectorSize; j++)
			probabilities[i][j] = double(rand()) / RAND_MAX;
	}

	auto z = new double*[classSize];
	for (auto i = 0; i < classSize; i++)
		z[i] = new double[n];

	auto isEnd = false;
	auto likelihoodLast = 0.0;
	auto iteration = 1;
	while (!isEnd)
	{
		//E
		for (auto i = 0; i < n; i++) {
			auto s = 0.0;
			auto zLogMin = 0.0;
			for (auto j = 0; j < classSize; j++) {
				auto zLog = log10(weights[j]);
				for (auto k = 0; k < vectorSize; k++) {
					if (data[i][k])
						zLog += log10(probabilities[j][k]);
					else
						zLog += log10(1.0 - probabilities[j][k]);
				}
				if (zLogMin == 0 || zLogMin > abs(zLog))
					zLogMin = abs(zLog);
			}
			for (auto j = 0; j < classSize; j++) {
				z[j][i] = weights[j];
				auto mult10 = zLogMin;
				for (auto k = 0; k < vectorSize; k++) {
					if (data[i][k])
						z[j][i] *= probabilities[j][k];
					else
						z[j][i] *= (1.0 - probabilities[j][k]);
					while (z[j][i] < 1 && mult10 >= 1) {
						z[j][i] *= 10;
						mult10--;
					}
					if (z[j][i] < 1.E-10) z[j][i] = 1.E-10;
				}
				z[j][i] *= pow(10.0, mult10);
				s += z[j][i];
			}
			for (auto j = 0; j < classSize; j++)
				z[j][i] /= s;
			zLogMin = 0.0;
			for (auto j = 0; j < classSize; j++)
				if (z[j][i] != 0)
					if (zLogMin = 0 || zLogMin > abs(log10(z[j][i])))
						zLogMin = abs(log10(z[j][i]));
			for (auto j = 0; j < classSize; j++)
				z[j][i] *= pow(10.0, zLogMin);
		}

		//Likelihood
		auto likelihood = 0.0;
		for (auto i = 0; i < classSize; i++)
			for (auto j = 0; j < n; j++) {
				auto s = log(weights[i]);
				for (auto k = 0; k < vectorSize; k++) {
					double value;
					if (data[j][k])
						value = probabilities[i][k];
					else
						value = (1.0 - probabilities[i][k]);
					if (value == 0)
						s += 1.E-100;
					else
						s += log(value);
				}
				likelihood += s*z[i][j];
			}
		printf("Iteration #%-3d: Log likelihood %-12.5lf\n", iteration, likelihood);
		if (abs(likelihood - likelihoodLast) < 0.0001) isEnd = true;
		likelihoodLast = likelihood;

		//M - weights
		for (auto i = 0; i < classSize; i++) {
			weights[i] = 0;
			for (auto j = 0; j < n; j++)
				weights[i] += z[i][j];
			weights[i] /= n;
		}

		//M - probabilities
		for (auto i = 0; i < classSize; i++) {
			for (auto j = 0; j < vectorSize; j++)
				probabilities[i][j] = 0;
			auto s = 0.0;
			for (auto j = 0; j < n; j++) {
				s += z[i][j];
				for (auto k = 0; k < vectorSize; k++)
					if (data[j][k])
						probabilities[i][k] += z[i][j];
			}
			for (auto j = 0; j < vectorSize; j++)
				probabilities[i][j] /= s;
		}

		//SaveModel((std::to_string(iteration) + " " + std::to_string(likelihood)).c_str());
		iteration++;
	}

	for (auto i = 0; i < classSize; i++)
		delete[] z[i];
	delete[] z;

	TestModel(test);
}


void ClasterizerEMBernoulli::SaveModel(const char* modelPath) {
	FILE* f;
	fopen_s(&f, modelPath, "wb");
	fwrite("MUZ", 1, 3, f);
	fwrite(&classSize, 4, 1, f);
	fwrite(&vectorSize, 4, 1, f);
	fwrite(weights, sizeof(double), classSize, f);
	for (auto i = 0; i < classSize; i++)
		fwrite(probabilities[i], sizeof(double), vectorSize, f);
	fclose(f);
}


void ClasterizerEMBernoulli::TestModel(IReader* test) {
	corpusSize = test->GetCorpusSize();
	auto data = test->GetData();
	labels = new char[corpusSize];

	for (auto i = 0; i < corpusSize; i++) {
		auto zLogMinClass = 0;
		auto zLogMin = 0.0;
		for (auto j = 0; j < classSize; j++) {
			auto zLog = log10(weights[j]);
			for (auto k = 0; k < vectorSize; k++) {
				if (data[i][k])
					zLog += log10(probabilities[j][k]);
				else
					zLog += log10(1.0 - probabilities[j][k]);
			}
			if (zLogMin == 0 || zLogMin > abs(zLog)) {
				zLogMin = abs(zLog);
				zLogMinClass = j;
			}
		}
		labels[i] = char(zLogMinClass);
	}
}


ClasterizerEMBernoulli::~ClasterizerEMBernoulli() {
	delete[] labels;
	labels = nullptr;
}


int ClasterizerEMBernoulli::GetCorpusSize() const {
	return corpusSize;
}


const char* ClasterizerEMBernoulli::GetLabels() const {
	return labels;
}
