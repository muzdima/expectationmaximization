#include "ReaderDummy.h"
#include <vector>


ReaderDummy::ReaderDummy(int classes, int corpusSize, int vectorSize, double delta) :corpusSize(corpusSize), vectorSize(vectorSize), classes(classes) {
	labels = new char[corpusSize];
	data = new unsigned char*[corpusSize];
	for (auto i = 0; i < corpusSize; i++)
		data[i] = new unsigned char[vectorSize];

	auto m = new unsigned char*[classes];
	for (auto i = 0; i < classes; i++)
		m[i] = new unsigned char[vectorSize];

	for (auto i = 0; i < classes; i++)
		for (auto j = 0; j < vectorSize; j++)
			m[i][j] = unsigned char(rand() % 256);

	for (auto i = 0; i < corpusSize; i++) {
		labels[i] = char(rand() % classes);
		for (auto j = 0; j < vectorSize; j++) {
			auto x = m[labels[i]][j] + 2.0*delta*rand() / RAND_MAX - delta;
			if (x < 0) x = 0;
			if (x > 255) x = 255;
			data[i][j] = unsigned char(x);
		}
	}

	for (auto i = 0; i < classes; i++)
		delete[] m[i];
	delete[] m;
}


ReaderDummy::~ReaderDummy() {
	delete[] labels;
	labels = nullptr;

	for (auto i = 0; i < corpusSize; i++)
		delete[] data[i];
	delete[] data;
	data = nullptr;
}


int ReaderDummy::GetCorpusSize() const {
	return corpusSize;
}


int ReaderDummy::GetVectorSize() const {
	return vectorSize;
}


const unsigned char* const* ReaderDummy::GetData() const {
	return data;
}


const char* ReaderDummy::GetLabels() const {
	return labels;
}


int ReaderDummy::GetClasses() const {
	return classes;
}