#pragma once
#include "IClasterizer.h"
#include "IReader.h"


class ClasterizerKMeans : public IClasterizer
{
public:
	ClasterizerKMeans(const char* modelPath, IReader* test);
	ClasterizerKMeans(IReader* train, IReader* test);
	virtual ~ClasterizerKMeans();
	int GetCorpusSize() const;
	const char* GetLabels() const;
	void SaveModel(const char* modelPath);
private:
	int corpusSize;
	char* labels;

	int classSize;
	int vectorSize;
	double** means;

	void TestModel(IReader* test);
};

