#pragma once


class IClasterizer
{
public:
	virtual int GetCorpusSize() const = 0;
	virtual const char* GetLabels() const = 0;
	virtual ~IClasterizer() {}
};

