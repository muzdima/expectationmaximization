#include <stdio.h>
#include <stdlib.h>
#include "ReaderMnist.h"
#include "ReaderBinary.h"
#include "ReaderDummy.h"
#include "ClasterizerKMeans.h"
#include "ClasterizerEMGaussian.h"
#include "ClasterizerENBernoulli.h"
#include "MapperSimple.h"
#include <fstream>
#include "Extra.h"
#include <time.h>


#define USING_KMEANS
#define USING_EM_BERNOULLI_MIXURE
#define USING_EM_GAUSSIAN_MIXURE


template<class T> IClasterizer* LoadClasterizer(IReader* readerTest, const char* modelPath = nullptr, unsigned char threshold = 0) {
	IClasterizer* clasterizer = nullptr;
	bool isLoad = false;
	if (modelPath != nullptr) {
		printf("Checking model\n");
		std::ifstream f;
		f.open(modelPath);
		if (f.is_open()) {
			f.close();
			printf("Model found\n");

			printf("Loading model\n");
			clasterizer = (IClasterizer*)(new T(modelPath, readerTest));
			isLoad = true;
		}
	}
	if (!isLoad) {
		if (modelPath != nullptr)
			printf("Model missing\n");

		printf("Reading train data\n");
		auto readerTrain = (IReader*)new ReaderMnist(true);

		if (threshold != 0) {
			printf("Binarizating train data\n");
			auto readerTrainRaw = readerTrain;
			readerTrain = new ReaderBinary(readerTrain, threshold);

			printf("Free raw train data\n");
			delete readerTrainRaw;
		}

		printf("Training model\n");
		clasterizer = (IClasterizer*)new T(readerTrain, readerTest);

		if (modelPath != nullptr) {
			printf("Saving model\n");
			((T*)clasterizer)->SaveModel(modelPath);
		}

		printf("Free train data\n");
		delete readerTrain;
	}
	return clasterizer;
}


void TestClasterizer(IClasterizer* clasterizer, IReader* readerTest, const char* labelsPath) {
	printf("Mapping classes with labels\n");
	IMapper* mapper = new MapperSimple(readerTest, clasterizer);

	printf("Confusion matrix:\n");
	PrintConfusionMatrix(mapper);
	printf("Error rates:\n");
	PrintErrorRates(mapper);

	printf("Writing labels to %s\n", labelsPath);
	WriteLabels(labelsPath, clasterizer, mapper);

	printf("Free mapper\n");
	delete mapper;
}


int main()
{
	srand(unsigned int(time(nullptr)));

	const char* modelPathKMeans = "../data/KMeans.model";
	const char* labelsPathKMeans = "../data/KMeans-result-labels.idx1-ubyte";

	const char* modelPathEMBernoulli = "../data/EMBernoulli.model";
	const char* labelsPathEMBernoulli = "../data/EMBernoulli-result-labels.idx1-ubyte";

	const char* modelPathEMGaussian = "../data/EMGaussian.model";
	const char* labelsPathEMGaussian = "../data/EMGaussian-result-labels.idx1-ubyte";

	const unsigned char threshold = 120;

	printf("Reading test data\n");
	auto readerTest = (IReader*)new ReaderMnist(false);

	printf("Binarizating test data\n");
	auto readerTestBinary = (IReader*)new ReaderBinary(readerTest, threshold);

	IClasterizer* clasterizer = nullptr;

#ifdef USING_KMEANS
	printf("\n--Clasterizer Kmeans:\n");
	clasterizer = LoadClasterizer<ClasterizerKMeans>(readerTest, modelPathKMeans);
	TestClasterizer(clasterizer, readerTest, labelsPathKMeans);
	printf("Free clasterizer\n");
	delete clasterizer;
#endif
#ifdef USING_EM_BERNOULLI_MIXURE
	printf("\n--Clasterizer EM Bernoulli:\n");
	clasterizer = LoadClasterizer<ClasterizerEMBernoulli>(readerTestBinary, modelPathEMBernoulli, threshold);
	TestClasterizer(clasterizer, readerTestBinary, labelsPathEMBernoulli);
	printf("Free clasterizer\n");
	delete clasterizer;
#endif
#ifdef USING_EM_GAUSSIAN_MIXURE
	printf("\n--Clasterizer EM Gaussian:\n");
	clasterizer = LoadClasterizer<ClasterizerEMGaussian>(readerTest, modelPathEMGaussian);
	TestClasterizer(clasterizer, readerTest, labelsPathEMGaussian);
	printf("Free clasterizer\n");
	delete clasterizer;
#endif

	printf("Free test data\n");
	delete readerTest;
	delete readerTestBinary;
	
	system("pause");
	return 0;
}
