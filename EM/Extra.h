#pragma once
#include "IMapper.h"
#include "IClasterizer.h"


void PrintConfusionMatrix(IMapper* mapper);
void PrintErrorRates(IMapper* mapper);
void WriteLabels(const char* path, IClasterizer* clasterizer, IMapper* mapper);