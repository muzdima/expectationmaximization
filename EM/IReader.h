#pragma once


class IReader
{
public:
	virtual int GetCorpusSize() const = 0;
	virtual int GetVectorSize() const = 0;
	virtual const unsigned char* const* GetData() const = 0;
	virtual const char* GetLabels() const = 0;
	virtual int GetClasses() const = 0;
	virtual ~IReader() {}
};

