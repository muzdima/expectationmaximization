#include "Extra.h"
#include <stdio.h>


void PrintConfusionMatrix(IMapper* mapper) {
	auto size = mapper->GetLabelListSize();
	auto labels = mapper->GetLabelList();
	auto matrix = mapper->GetConfusionMatrix();
	printf("\n");
	printf("   ");
	for (auto i = 0; i < size; i++)
		printf("    %c", labels[i] + '0');
	printf("\n");
	printf("   ");
	for (auto i = 0; i < size; i++)
		printf("    |");
	printf("\n");
	for (auto i = 0; i < size; i++) {
		printf("%c -", labels[i] + '0');
		for (auto j = 0; j < size; j++)
			printf(" %4d", matrix[i][j]);
		printf("\n");
	}
}


void PrintErrorRates(IMapper* mapper) {
	auto size = mapper->GetLabelListSize();
	auto labels = mapper->GetLabelList();
	auto matrix = mapper->GetConfusionMatrix();
	printf("\n");
	printf(" Type 1 error    Type 2 error\n");
	printf("          |        |\n");
	for (auto i = 0; i < size; i++) {
		auto fn = 0;
		for (auto j = 0; j < size; j++)
			if (j != i)
				fn += matrix[i][j];
		auto fp = 0;
		auto fa = 0;
		for (auto j = 0; j < size; j++)
			if (j != i) {
				fp += matrix[j][i];
				for (auto k = 0; k < size; k++)
					fa += matrix[j][k];
			}
		printf("%c - %7.4lf%% %7.4lf%%\n", labels[i] + '0', 100.0*fn / (fn + matrix[i][i]), 100.0*fp / fa);
	}
}


void WriteLabels(const char* path, IClasterizer* clasterizer, IMapper* mapper) {
	FILE* f;
	fopen_s(&f, path, "wb");
	int ib;
	ib = 2049;
	fwrite(&ib, 4, 1, f);
	ib = clasterizer->GetCorpusSize();
	fwrite(&ib, 4, 1, f);
	auto labels = clasterizer->GetLabels();
	unsigned char ic;
	for (auto i = 0; i < ib; i++) {
		ic = mapper->MapClasterizerToReaderLabel(labels[i]);
		fwrite(&ic, 1, 1, f);
	}
	fclose(f);
}

