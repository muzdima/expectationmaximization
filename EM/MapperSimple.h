#pragma once
#include "IMapper.h"
#include "IReader.h"
#include "IClasterizer.h"
#include <map>


class MapperSimple : public IMapper
{
public:
	MapperSimple(IReader* reader, IClasterizer* clasterizer);
	virtual ~MapperSimple();
	int GetLabelListSize() const;
	char MapClasterizerToReaderLabel(char label) const;
	const char* GetLabelList() const;
	const int* const* GetConfusionMatrix() const;
private:
	IReader* reader;
	IClasterizer* clasterizer;
	std::map<char, char> clasterizerToReaderLabel;
	int labelListSize;
	char* labelListReader;
	int** matrix;
};

