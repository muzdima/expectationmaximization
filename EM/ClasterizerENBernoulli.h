#pragma once
#include "IClasterizer.h"
#include "IReader.h"


class ClasterizerEMBernoulli : public IClasterizer
{
public:
	ClasterizerEMBernoulli(const char* modelPath, IReader* test);
	ClasterizerEMBernoulli(IReader* train, IReader* test);
	virtual ~ClasterizerEMBernoulli();
	int GetCorpusSize() const;
	const char* GetLabels() const;
	void SaveModel(const char* modelPath);
private:
	int corpusSize;
	char* labels;

	int classSize;
	int vectorSize;
	double* weights;
	double** probabilities;

	void TestModel(IReader* test);
};

