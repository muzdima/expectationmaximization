#pragma once
#include "IReader.h"


class ReaderDummy : public IReader
{
public:
	ReaderDummy(int classes, int corpusSize, int vectorSize, double delta);
	virtual ~ReaderDummy();
	int GetCorpusSize() const;
	int GetVectorSize() const;
	const unsigned char* const* GetData() const;
	const char* GetLabels() const;
	int GetClasses() const;
private:
	int corpusSize, vectorSize;
	unsigned char** data;
	char* labels;
	int classes;
};

